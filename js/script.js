
  	let prevTime, stopwatchInterval, elapsedTime = 0; 
    let updateTime = function() { 
        let tempTime = elapsedTime; 
        let milliseconds = tempTime % 1000;
        if (milliseconds<10){
            milliseconds=(`00${milliseconds}`);
        } else{
            if (milliseconds<100){
                milliseconds=(`0${milliseconds}`);
            };
        };
        tempTime = Math.floor(tempTime/1000); 
        let seconds = tempTime % 60; 
        tempTime = Math.floor(tempTime/60); 
        let minutes = tempTime % 60; 
        tempTime = Math.floor(tempTime/60); 
        let hours = tempTime % 60; 
        let time = hours + " : " + minutes + " : " + seconds + " : " + milliseconds; 
        $("#time").text(time); 
    }; 
    $("#startButton").click(function() { 
        if (!stopwatchInterval) { 
        stopwatchInterval = setInterval(function() { 
         if (!prevTime) { 
         prevTime = Date.now(); 
         } 
         elapsedTime += Date.now() - prevTime; 
         prevTime = Date.now(); 
         updateTime(); 
        }, 50); 
        } 
    }); 
    $("#pauseButton").click(function() { 
        if (stopwatchInterval) { 
        clearInterval(stopwatchInterval); 
        stopwatchInterval = null; 
        } 
        prevTime = null; 
    }); 
    $("#resetButton").click(function() { 
        $('#pauseButton').css('display','none');
            $("#startButton").css('display','block');
        if (stopwatchInterval) { 
            clearInterval(stopwatchInterval); 
            stopwatchInterval = null;  } 
            prevTime = null; 
        elapsedTime = 0; 
        updateTime(); 
    });  
    $(document).ready(function() { 
     updateTime(); 
     });
    $('#startButton').click(function v(){
      $('#startButton').toggle( function k(){
          $(this).css('display','none');
          $("#pauseButton").css('display','block');
      })});
      $('#pauseButton').click(function v(){
        $('#pauseButton').toggle( function k(){
            $(this).css('display','none');
            $("#startButton").css('display','block');
        })});